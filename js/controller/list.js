function list() {
  angular.module("quizApp").controller("listQuiz", listController);

  listController.$inject = ["quizMetrics", "DataService"];

  function listController(quizMetrics, DataService) {
    var ls = this;

    ls.quizMetrics = quizMetrics;
    ls.data = DataService.turtlesData;
    ls.activeTurtle = {};
    ls.changeActiveTurtle = changeActiveTurtle;
    ls.search = "";
    ls.activeQuiz = activeQuiz;

    function changeActiveTurtle(index) {
      ls.activeTurtle = index;
    }

    function activeQuiz(id) {
      quizMetrics.changeProps(id);
      quizMetrics.changeState("quiz", true);
    }
  }
}

list();
