function Quiz() {
  angular.module("quizApp").controller("quizCtrl", QuizController);

  QuizController.$inject = ["quizMetrics", "DataService"];

  function QuizController(quizMetrics, DataService) {
    var ls = this;

    ls.quizMetrics = quizMetrics;
    ls.DataService = DataService;
    ls.questionsAnswer = questionsAnswer;
    ls.activeQuestion = 0;
    ls.setActiveQuestion = setActiveQuestion;
    ls.quiz = ADAV;
    ls.selectAnswer = selectAnswer;
    ls.error = false;
    ls.finalise = false;
    ls.finaliseAnswer = finaliseAnswer;

    var numQuestionAnswer = 0;

    function setActiveQuestion(index) {
      if (index === undefined) {
        var breakOut = false;
        var quizLength = ls.quiz.length - 1;

        while (!breakOut) {
          ls.activeQuestion =
            ls.activeQuestion < quizLength ? ++ls.activeQuestion : 0;

          if (ls.activeQuestion === 0) {
            ls.error = true;
          }

          if (ls.quiz[ls.activeQuestion].selected === null) {
            breakOut = true;
          }
        }
      } else {
        ls.activeQuestion = index;
      }
    }

    function questionsAnswer() {
      console.log(quizMetrics.listQuiz);
      var quizLength = ls.quiz.length;
      if (ls.quiz[ls.activeQuestion].selected !== null) {
        numQuestionAnswer++;
        if (numQuestionAnswer >= quizLength) {
          // finalize quiz
          for (var i = 0; i < quizLength; i++) {
            if (ls.quiz[i].selected === null) {
              setActiveQuestion(i);
              return;
            }
          }
          ls.error = false;
          ls.finalise = true;
          return;
        }
      }

      ls.setActiveQuestion();
    }

    function selectAnswer(index) {
      ls.quiz[ls.activeQuestion].selected = index;
    }

    function finaliseAnswer() {
      ls.finalise = false;
      numQuestionAnswer = 0;
      ls.activeQuestion = 0;
      quizMetrics.markQuiz();
      quizMetrics.changeState("quiz", false);
      quizMetrics.changeState("results", true);
    }
  }

  var ADAV = [
    {
      type: "text",
      text: "Có mấy loại Service?",
      possibilities: [
        {
          answer: "1"
        },
        {
          answer: "2"
        },
        {
          answer: "3"
        },
        {
          answer: "4"
        }
      ],
      selected: null,
      correct: null
    },
    {
      type: "text",
      text:
        "Trong IntentService, phương thức onHandlerIntent sẽ được tự động gọi trong phương thức nào?",
      possibilities: [
        {
          answer: "onServiceConnected()"
        },
        {
          answer: "onServiceDisConnected()"
        },
        {
          answer: "onStartCommand()"
        },
        {
          answer: "onBind()"
        }
      ],
      selected: null,
      correct: null
    },
    {
      type: "text",
      text:
        "Khi nào phương thức ServiceConnection.onServiceConnected được gọi?",
      possibilities: [
        {
          answer: "Sau khi một thành phần gọi Context.startService()"
        },
        {
          answer: "Sau khi một thành phần gọi Context.bindService()"
        },
        {
          answer:
            "Sau khi BroadcastReceiver nhận một Intent được gửi bởi Service"
        },
        {
          answer: "Khi một Service gọi Context.startActivity()"
        }
      ],
      selected: null,
      correct: null
    },
    {
      type: "text",
      text:
        "Phương thức Service.stopSelf(int startId) có tham số kiểu int. Tham số này dùng để làm gì?",
      possibilities: [
        {
          answer: "Nếu startId khác 0, Service sẽ bị hủy một cách vô điều kiện"
        },
        {
          answer:
            "Tham số miêu tả số miliseconds bị trì hoãn trước khi Service bị hủy"
        },
        {
          answer:
            "Là start identifier gần nhất nhận được bởi lời gọi onStart(Intent, int)"
        },
        {
          answer:
            "Được sử dụng để dừng Service nếu Service đang chạy trong process id với process id bằng giá trị startid"
        }
      ],
      selected: null,
      correct: null
    },
    {
      type: "text",
      text: "Câu nào là đúng khi đề cập đến Service?'",
      possibilities: [
        {
          answer:
            "Các ứng dụng khác nhau không thể truy cập đến các Service của chính nó"
        },
        {
          answer: "Tất cả phương án đều đúng"
        },
        {
          answer:
            "Lớp Service luôn luôn được truy cập bởi tất cả ứng dụng Android khác cài trên thiết bị"
        },
        {
          answer:
            "Service có thể chạy nền vô hạn kể cả khi thành phần khởi tạo Service đã bị hủy"
        }
      ],
      selected: null,
      correct: null
    },
    {
      type: "text",
      text: "Trong IntentService, onBind mặc định trả lại giá trị nào?",
      possibilities: [
        {
          answer: "-1"
        },
        {
          answer: "null"
        },
        {
          answer: "0"
        },
        {
          answer: '""'
        }
      ],
      selected: null,
      correct: null
    },
    {
      type: "text",
      text: "Phương thức stopSelf dùng để làm gì?",
      possibilities: [
        {
          answer: "Dừng thông báo notification tới người dùng"
        },
        {
          answer: "Dừng chương trình"
        },
        {
          answer: "Dừng kết nối Service với Sqlite"
        },
        {
          answer: "Dừng Service"
        }
      ],
      selected: null,
      correct: null
    },
    {
      type: "text",
      text: "Phương thức onStartCommand được gọi khi nào?",
      possibilities: [
        {
          answer: "Khi phương thức bindService được gọi"
        },
        {
          answer: "Khi phương thức stopSelf được gọi"
        },
        {
          answer: "Khi phương thức onBind được gọi"
        },
        {
          answer: "Khi phương thức startService được gọi"
        }
      ],
      selected: null,
      correct: null
    },
    {
      type: "text",
      text:
        "Bạn nên giải phóng tài nguyên mà Service sử dụng trong phương thức nào?",
      possibilities: [
        {
          answer: "onPause"
        },
        {
          answer: "pauseService"
        },
        {
          answer: "onDestroy"
        },
        {
          answer: "startService"
        }
      ],
      selected: null,
      correct: null
    },
    {
      type: "text",
      text:
        "Làm thế nào để disable một Broadcast Receiver khi nó đã được đăng ký trong AndroidManifest.xml?",
      possibilities: [
        {
          answer: "Sử dụng lớp PackageReceiverManager"
        },
        {
          answer: "Sử dụng lớp BroadcastReceiver"
        },
        {
          answer: "Sử dụng lớp BroadcastReceiverManger"
        },
        {
          answer: "Sử dụng lớp PackageManager"
        }
      ],
      selected: null,
      correct: null
    }
  ];
}
//13

Quiz();
