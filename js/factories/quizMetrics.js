(function() {
  angular.module("quizApp").factory("quizMetrics", QuizMetrics);

  function QuizMetrics() {
    var quizObj = {
      quizActive: false,
      changeState: changeState,
      resultsActive: false,
      changeProps: changeProps,
      listQuiz: ""
    };

    function changeState(metric, state) {
      if (metric === "quiz") {
        quizObj.quizActive = state;
      } else if (metric === "results") {
        quizObj.resultsActive = state;
      } else {
        return false;
      }
    }

    function changeProps(props) {
      quizObj.listQuiz = props;
    }

    return quizObj;
  }
})();
